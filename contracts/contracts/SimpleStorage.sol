pragma solidity >=0.4.21 <0.6.0;

contract SimpleStorage {
    string public value;

    function set(string _value) public {
        value = _value;
    }
}
