const Web3 = require('web3');
const express = require('express');
const { abi: contractABI, bytecode: contractBytecode } = require('../contracts/build/contracts/SimpleStorage.json')

const { RPC_ENDPOINT, PRIVATE_KEY_HEX } = process.env

const app = express();
const web3 = new Web3(RPC_ENDPOINT);

const account = web3.eth.accounts.wallet.add(PRIVATE_KEY_HEX)
const fromAddress = account.address
let contract

app.post('/', async (req, res) => {
    const { value } = req.query
    await contract.methods.set(value).send({ from: fromAddress, gas: 300000 })
    console.log(`Stored value ${value} to SimpleStorage contract`)
    res.sendStatus(200)
});

app.get('/', async (req, res) => {
    const value = await contract.methods.value().call()
    console.log(`Retrieved value ${value} from SimpleStorage contract`)
    res.send(value)
});

const start = async () => {
    contract = await new web3.eth.Contract(contractABI)
        .deploy({
            data: contractBytecode
        })
        .send({
            from: fromAddress,
            gas: 300000
        })

    console.log(`SimpleStorage deployed to ${contract.options.address}`)

    app.listen(3000, () => console.log('API listening on port 3000!'))
}

start() 
